package ru.t1.stepanishchev.tm.api.controller;

public interface ITaskController {

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTasks();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void completeTaskById();

    void completeTaskByIndex();

}

