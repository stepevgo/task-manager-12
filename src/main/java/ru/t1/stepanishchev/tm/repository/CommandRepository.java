package ru.t1.stepanishchev.tm.repository;

import ru.t1.stepanishchev.tm.api.repository.ICommandRepository;
import ru.t1.stepanishchev.tm.constant.ArgumentConst;
import ru.t1.stepanishchev.tm.constant.TerminalConst;
import ru.t1.stepanishchev.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show about program.");

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show program version.");

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show list arguments.");

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system information.");

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show command list.");

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list.");

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show list projects.");

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects.");

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project.");

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Display project by id.");

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Display project by index.");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id.");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index.");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id.");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index.");

    public static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index.");

    public static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id.");

    public static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index.");

    public static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id.");

    public static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index.");

    public static final Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id.");

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show list tasks.");

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks.");

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task.");

    public static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Display task by id.");

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Display task by index.");

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id.");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index.");

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id.");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index.");

    public static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index.");

    public static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id.");

    public static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index.");

    public static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id.");

    public static final Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index.");

    public static final Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete task by id.");

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application.");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {
            INFO, ABOUT, VERSION, HELP,
            ARGUMENTS, COMMANDS,

            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_START_BY_INDEX, TASK_START_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_COMPLETE_BY_ID,


            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_START_BY_INDEX, PROJECT_START_BY_ID,
            PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_COMPLETE_BY_ID,


            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}